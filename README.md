# butterfly-spring-boot-starter

## dynamic add extra property to entity

+ 本版本实现动态添加属性
    - 属性可以设置默认值，所有都会设计默认值
    - 属性可以动态获取值，需要配置对应的表、关联字段、添加属性对应表字段、关联属性对应表字段后，即可动态设置
    

+ 之前有试过，添加多个字段，但觉得实际需求并不多，就去掉了，后面再考虑添加上！！！

+ 多属性支持已经添加！！！