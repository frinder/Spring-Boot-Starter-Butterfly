package com.frinder.cglib;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author frinder
 * @date 2017/10/16
 */
public abstract class AbstractExtraPropertyAdapter implements ExtraPropertyAdapter {

    @Autowired
    protected AdvisorTemplate advisorTemplate;

    @Override
    public Object getResult(ExtraProperty extraProperty, Object dest) {
        return dest;
    }

    @Override
    public Object getResult(ExtraProperties extraProperties, Object dest) {
        return dest;
    }
}
