package com.frinder.cglib;

import com.frinder.temp.Result;
import com.frinder.temp.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 *
 * @author frinder
 * @date 2017/10/14
 */
@Component
public class DefaultExtraPropertyAdapter extends AbstractExtraPropertyAdapter {

    @Override
    public Object getResult(ExtraProperties extraProperties, Object dest) {
        if (dest instanceof User) {
            dest = advisorTemplate.getTarget4MultipleProperty(extraProperties, dest);
        } else if (dest instanceof List) {
            dest = advisorTemplate.getTargetList4MultipleProperty(extraProperties, (List<Object>) dest);
        } else if (dest instanceof Result) {
            Result result = (Result) dest;
            Object target = result.getData();
            if (target instanceof User) {
                target = advisorTemplate.getTarget4MultipleProperty(extraProperties, result.getData());
            } else if (target instanceof List) {
                target = advisorTemplate.getTargetList4MultipleProperty(extraProperties, (List<Object>) result.getData());
            }
            result.setData(target);
        }
        return dest;
    }

    @Override
    public Object getResult(ExtraProperty extraProperty, Object dest) {
        if (dest instanceof User) {
            dest = advisorTemplate.getTarget(extraProperty, dest);
        } else if (dest instanceof List) {
            dest = advisorTemplate.getTargetList(extraProperty, (List<Object>) dest);
        } else if (dest instanceof Result) {
            Result result = (Result) dest;
            Object target = result.getData();
            if (target instanceof User) {
                target = advisorTemplate.getTarget(extraProperty, result.getData());
            } else if (target instanceof List) {
                target = advisorTemplate.getTargetList(extraProperty, (List<Object>) result.getData());
            }
            result.setData(target);
        }
        return dest;
    }

}
