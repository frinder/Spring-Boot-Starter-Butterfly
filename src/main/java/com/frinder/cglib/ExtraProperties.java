package com.frinder.cglib;

import java.lang.annotation.*;

/**
 *
 * @author frinder
 * @date 2017/10/19
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExtraProperties {

    ExtraProperty[] extraProperties();

}
