package com.frinder.cglib;

import java.lang.annotation.*;

/**
 *
 * @author frinder
 * @date 2017/7/19
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExtraProperty {

    // 目标属性名，如：userName（请使用驼峰命名法）
    String name();

    // 目标属性默认值，如：frinder
    String value() default "";

    // 目标属性关联属性，如：sid，则可以根据 sid 在表 “tableName()” 中查询出 userName 值
    String refSid() default "";

    // 关联表名
    String tableName() default "";

    // 目标属性对应表中字段名
    String fieldName() default "";

    // 目标属性关联属性对应表中字段名
    String refFieldName() default "";

}
