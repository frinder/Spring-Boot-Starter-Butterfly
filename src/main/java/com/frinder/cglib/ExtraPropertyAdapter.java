package com.frinder.cglib;

/**
 *
 * @author frinder
 * @date 2017/10/14
 */
public interface ExtraPropertyAdapter {

    Object getResult(ExtraProperty extraProperty, Object dest);

    Object getResult(ExtraProperties extraProperties, Object dest);

}
