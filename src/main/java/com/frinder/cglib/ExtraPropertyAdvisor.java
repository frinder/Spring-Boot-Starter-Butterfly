package com.frinder.cglib;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author frinder
 * @date 2017/7/19
 * <p>
 * 添加额外属性
 */
@Component
@Aspect
@Slf4j
public class ExtraPropertyAdvisor {

    @Autowired(required = false)
    private ExtraPropertyAdapter adapter;

    @Around(value = "@annotation(extraProperties)")
    public Object extraProperties(ProceedingJoinPoint point, ExtraProperties extraProperties) throws RuntimeException {
        try {
            // run program
            Object result = point.proceed();
            if (null == result) return result;
            if (null == adapter) return result;
            log.info("METHODS : {}", AdvisorTemplate.METHODS);
            log.info("CACHE : {}", AdvisorTemplate.CACHE);
            log.info("ExtraProperties : {}", extraProperties);
            // handle
            result = adapter.getResult(extraProperties, result);
            return result;
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
            throw new RuntimeException(t.getMessage(), t);
        }
    }


    @Around(value = "@annotation(extraProperty)")
    public Object extraProperty(ProceedingJoinPoint point, ExtraProperty extraProperty) throws RuntimeException {
        try {
            // run program
            Object result = point.proceed();
            if (null == result) return result;
            if (null == adapter) return result;
            log.info("METHODS : {}", AdvisorTemplate.METHODS);
            log.info("CACHE : {}", AdvisorTemplate.CACHE);
            log.info("ExtraProperty : {}", extraProperty);
            // handle
            result = adapter.getResult(extraProperty, result);
            return result;
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
            throw new RuntimeException(t.getMessage(), t);
        }
    }


}
