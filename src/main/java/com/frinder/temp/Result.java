package com.frinder.temp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by frinder on 2017/10/14.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Result {

    private int code;
    private Object data;

}
