package com.frinder.web;

import com.frinder.cglib.ExtraProperties;
import com.frinder.cglib.ExtraProperty;
import com.frinder.temp.Result;
import com.frinder.temp.User;
import com.google.common.collect.Lists;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by frinder on 2017/10/16.
 */
@RestController
@RequestMapping("/user2")
public class User2Controller {

    User user1 = User.builder().id(1L).userName("aaa").password("aaa").nickName("aaa").sex(1).build();
    User user2 = User.builder().id(2L).userName("bbb").password("bbb").nickName("bbb").sex(2).build();

    List<User> users = Lists.newArrayList(user1, user2);

    Result result1 = Result.builder().code(200).data(users).build();

    Result result2 = Result.builder().code(200).data(user1).build();

    /***********************************************************************/
    /** 返回原始 User 类型 */
    /***********************************************************************/

    @GetMapping("/user1")
    @ExtraProperties(extraProperties = {
            @ExtraProperty(name = "address", value = "GZ of China!"),
            @ExtraProperty(name = "phone", value = "18888888888")
    })
    public User user1() {
        return user1;
    }

    @GetMapping("/user2")
    @ExtraProperties(extraProperties = {
            @ExtraProperty(
                    name = "address",
                    refSid = "id",
                    tableName = "user_info",
                    fieldName = "address",
                    refFieldName = "user_sid"
            ),
            @ExtraProperty(
                    name = "phone",
                    refSid = "id",
                    tableName = "user_info",
                    fieldName = "phone",
                    refFieldName = "user_sid"
            )
    })
    public User user2() {
        return user2;
    }


    /***********************************************************************/
    /** 返回 List<User> 类型 */
    /***********************************************************************/

    @GetMapping("/user3")
    @ExtraProperties(extraProperties = {
            @ExtraProperty(name = "address", value = "GZ of China!"),
            @ExtraProperty(name = "phone", value = "18888888888")
    })
    public List<User> user3() {
        return users;
    }

    @GetMapping("/user4")
    @ExtraProperties(extraProperties = {
            @ExtraProperty(
                    name = "address",
                    refSid = "id",
                    tableName = "user_info",
                    fieldName = "address",
                    refFieldName = "user_sid"
            ),
            @ExtraProperty(
                    name = "phone",
                    refSid = "id",
                    tableName = "user_info",
                    fieldName = "phone",
                    refFieldName = "user_sid"
            )
    })
    public List<User> user4() {
        return users;
    }


    /***********************************************************************/
    /** 返回复合类型，List<User> */
    /***********************************************************************/

    @GetMapping("/user5")
    @ExtraProperties(extraProperties = {
            @ExtraProperty(name = "address", value = "GZ of China!"),
            @ExtraProperty(name = "phone", value = "18888888888")
    })
    public Result user5() {
        return result1;
    }


    @GetMapping("/user6")
    @ExtraProperties(extraProperties = {
            @ExtraProperty(
                    name = "address",
                    refSid = "id",
                    tableName = "user_info",
                    fieldName = "address",
                    refFieldName = "user_sid"
            ),
            @ExtraProperty(
                    name = "phone",
                    refSid = "id",
                    tableName = "user_info",
                    fieldName = "phone",
                    refFieldName = "user_sid"
            )
    })
    public Result user6() {
        return result1;
    }


    /***********************************************************************/
    /** 返回复合类型，User */
    /***********************************************************************/

    @GetMapping("/user7")
    @ExtraProperties(extraProperties = {
            @ExtraProperty(name = "address", value = "GZ of China!"),
            @ExtraProperty(name = "phone", value = "18888888888")
    })
    public Result user7() {
        return result2;
    }

    @GetMapping("/user8")
    @ExtraProperties(extraProperties = {
            @ExtraProperty(
                    name = "address",
                    refSid = "id",
                    tableName = "user_info",
                    fieldName = "address",
                    refFieldName = "user_sid"
            ),
            @ExtraProperty(
                    name = "phone",
                    refSid = "id",
                    tableName = "user_info",
                    fieldName = "phone",
                    refFieldName = "user_sid"
            )
    })
    public Result user8() {
        return result2;
    }

}
