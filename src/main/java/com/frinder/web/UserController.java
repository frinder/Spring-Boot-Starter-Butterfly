package com.frinder.web;

import com.frinder.cglib.ExtraProperty;
import com.frinder.temp.Result;
import com.frinder.temp.User;
import com.google.common.collect.Lists;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by frinder on 2017/10/16.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    User user1 = User.builder().id(1L).userName("aaa").password("aaa").nickName("aaa").sex(1).build();
    User user2 = User.builder().id(2L).userName("bbb").password("bbb").nickName("bbb").sex(2).build();

    List<User> users = Lists.newArrayList(user1, user2);

    Result result1 = Result.builder().code(200).data(users).build();

    Result result2 = Result.builder().code(200).data(user1).build();

    /***********************************************************************/
    /** 返回原始 User 类型 */
    /***********************************************************************/

    @GetMapping("/user1")
    @ExtraProperty(name = "address", value = "GZ of China!")
    public User user1() {
        return user1;
    }

    @GetMapping("/user2")
    @ExtraProperty(
            name = "address",
            refSid = "id",
            tableName = "user_info",
            fieldName = "address",
            refFieldName = "user_sid"
    )
    public User user2() {
        return user2;
    }


    /***********************************************************************/
    /** 返回 List<User> 类型 */
    /***********************************************************************/

    @GetMapping("/user3")
    @ExtraProperty(name = "address", value = "GZ of China!")
    public List<User> user3() {
        return users;
    }

    @GetMapping("/user4")
    @ExtraProperty(
            name = "address",
            refSid = "id",
            tableName = "user_info",
            fieldName = "address",
            refFieldName = "user_sid"
    )
    public List<User> user4() {
        return users;
    }

    @GetMapping("/user5")
    @ExtraProperty(name = "address", value = "GZ of China!")
    public Result user5() {
        return result1;
    }

    /***********************************************************************/
    /** 返回复合类型，List<User> */
    /***********************************************************************/

    @GetMapping("/user6")
    @ExtraProperty(
            name = "address",
            refSid = "id",
            tableName = "user_info",
            fieldName = "address",
            refFieldName = "user_sid"
    )
    public Result user6() {
        return result1;
    }


    /***********************************************************************/
    /** 返回复合类型，User */
    /***********************************************************************/

    @GetMapping("/user7")
    @ExtraProperty(name = "address", value = "GZ of China!")
    public Result user7() {
        return result2;
    }

    @GetMapping("/user8")
    @ExtraProperty(
            name = "address",
            refSid = "id",
            tableName = "user_info",
            fieldName = "address",
            refFieldName = "user_sid"
    )
    public Result user8() {
        return result2;
    }

}
